/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        console.log("start...");
        initFCM();
//        initPage();
    }
};

app.initialize();

function initFCM(){
    FCMPlugin.getToken(
        function(token){
            console.log(token);
            initPage(token);
        },
        function(err){
            console.log('error retrieving token: ' + err);
        }
    );
}
function initPage(token){
    var target = "_blank";
    var options = "location=no,toolbar=no,transitionstyle=crossdissolve,zoom=no,EnableViewPortScale=yes";
    var mainURL = "http://developer.connecttel.com/thecaleo/frontend/web/";
//    var mainURL = "http://thecaleo.local/";
    var url = mainURL+"?udid="+token+"&typeId=0";
    inAppBrowserRef = window.open(url, target, options);
    inAppBrowserRef.addEventListener('loadstart', loadStartCallBack);
    inAppBrowserRef.addEventListener('loadstop', loadStopCallBack);
}
function loadStartCallBack(){
    console.log("start loading a page ...");
    inAppBrowserRef.executeScript({ code: "sessionStorage.getItem('import')" }, function(values) {
        if(values && values.length > 0){
            if(values[0]=="import"){
                    contactsInit();
                    console.log(values);
                    inAppBrowserRef.executeScript({ code: "sessionStorage.setItem('import', '')" });
            }
        }
    });
}
function loadStopCallBack(){
    console.log("stop loading a page ...");
}
function contactsInit(){
    var options      = new ContactFindOptions();
    options.filter   = "";
    options.multiple = true;
    options.desiredFields = [navigator.contacts.fieldType.name,navigator.contacts.fieldType.emails,navigator.contacts.fieldType.phoneNumbers];
    options.hasPhoneNumber = true;
    var fields       = [navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name];
    navigator.contacts.find(fields, gotContacts, errorHandler, options);
}
function errorHandler(e){
    console.log("errorHandler: "+e);
}
function gotContacts(c){
    //console.log(c);
    var jsondata = [];
    var track = 0;
    for(var i=0, len=c.length; i<len; i++) {
        var data = c[i];
       console.log(data);
        if(data != null){
            var name = data['name'];
            var fullname =  "";
            if((name !=null)){
                //var firstName = (name['givenName'] != null) ?name['givenName']:"";
                //var lastName = (name['familyName'] != null) ?name['familyName']:"";
                fullname = name['formatted'];
            }
            var emailAddress = data['emails'];
            var emails = {};
            if (emailAddress != null && emailAddress.length > 0){
                for (var j=0; j<emailAddress.length; j++ ){
                    emails[j]=emailAddress[j]['value'];
                }
            }
            var phoneNumbers = data['phoneNumbers'];
            var phones = {};
            if (phoneNumbers != null && phoneNumbers.length > 0){
                for (var j=0; j<phoneNumbers.length; j++ ){
                    phones[j] = phoneNumbers[j]['value'];
                 }
            }
            var contactData = {'fullname':fullname,'emails':emails,'phones':phones};
            jsondata[track] = contactData;
            track++;
            if(track%100 == 0){
                track = 0;
                postJSON(jsondata);
                jsondata = [];
            }
        }
    }
    if(track%100 != 0){
        track = 0;
        postJSON(jsondata);
        jsondata = [];
    }
    console.log(jsondata.length);
}

function postJSON(contacts){
    var urlPost = "http://developer.connecttel.com/thecaleo/frontend/web/mobile/addcontacts";
//    var urlPost = "http://thecaleo.local/mobile/addcontacts";
    $.ajax({
           type: "POST",
           url: urlPost,
           crossDomain: true,
           cache: false,
           data: {
                contactData: contacts
           },
           success: function(result){
                console.log(result);
           },
           error:function(e){
                console.log(e);
           }
    });
}




